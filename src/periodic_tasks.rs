use crate::discord_handler::{get_merge_request_threads, GitlabProject, VELOREN_SERVER_ID};
use serde_json::{from_str, Value};
use serenity::{
    builder::{CreateMessage, EditThread},
    prelude::*,
};
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::RwLock;
use tracing::{error, log::info};

pub struct PeriodicTasker {
    pub client: reqwest::Client,
    pub gitlab_projects: HashMap<&'static str, GitlabProject>,
    pub context: Arc<RwLock<Option<Context>>>,
}

impl PeriodicTasker {
    async fn close_threads(&self) -> Result<(), Box<dyn std::error::Error>> {
        let context = self.context.read().await;
        if let Some(context) = &*context {
            for gitlab_project in self.gitlab_projects.values() {
                // Go through each thread and check if it's an MR thread
                let merge_requests = get_merge_request_threads(
                    context.cache.guild(VELOREN_SERVER_ID).unwrap().to_owned(),
                    gitlab_project,
                )?;

                for (merge_request_number, thread) in merge_requests.iter() {
                    let mut thread = thread.to_owned();

                    // Skip any already archived threads
                    if thread.thread_metadata.unwrap().archived {
                        continue;
                    }

                    // Query the GitLab API with the Veloren repo to find the MR
                    let request = self
                        .client
                        .get(format!(
                            "https://gitlab.com/api/v4/projects/{}/merge_requests/{}",
                            gitlab_project.id, merge_request_number
                        ))
                        .build()?;
                    let gitlab_response = self.client.execute(request).await?;
                    let gitlab_response_body = gitlab_response.text().await?;
                    let body_json: Value = from_str(&gitlab_response_body)?;

                    // Close any threads that are not open. This will only hit threads that
                    // specifically have `MR####` somewhere in their title.
                    if body_json["state"] == "merged" || body_json["state"] == "closed" {
                        // Log the thread name being closed
                        info!(
                            "Closing thread for MR {}: {} of {} because it is no longer open",
                            merge_request_number, body_json["title"], gitlab_project.name
                        );
                        // Send a message to the thread that it is being closed
                        thread
                            .send_message(&context.http, CreateMessage::new().content(format!("MR {}{}: {} is now in the '{}' state so this thread is being archived. Please message @AngelOnFira if this is causing undesirable behaviour!", gitlab_project.discord_thread_prefix, merge_request_number, body_json["title"], body_json["state"])))
                            .await?;

                        // Close the thread
                        thread
                            .edit_thread(&context.http, EditThread::new().archived(true))
                            .await?;
                    }
                }
            }
        }

        Ok(())
    }

    pub async fn run(self) -> Result<(), Box<dyn std::error::Error>> {
        loop {
            {
                if let Err(e) = self.close_threads().await {
                    error!(?e, "Error while closing threads");
                    return Err(e);
                }
            }
            tokio::time::sleep(Duration::from_secs(5 * 60)).await;
        }
    }
}
