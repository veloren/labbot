use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
};

use git_version::git_version;
use regex::Regex;
use serde_json::{from_str, Value};
use serenity::{
    all::{
        AutoArchiveDuration, CommandDataOption, CommandDataOptionValue, CommandInteraction,
        CommandOptionType, Interaction,
    },
    async_trait,
    builder::{
        CreateCommand, CreateCommandOption, CreateInteractionResponse,
        CreateInteractionResponseMessage, CreateMessage, CreateThread,
    },
    model::{
        channel::GuildChannel,
        gateway::Ready,
        guild::{Guild, PremiumTier},
        prelude::GuildId,
    },
    prelude::*,
    utils::MessageBuilder,
};
use tracing::{error, info, trace, warn};

#[derive(Clone)]
pub struct GitlabProject {
    pub name: String,
    pub id: String,
    pub discord_thread_prefix: String,
}

impl GitlabProject {
    pub fn new(name: &str, id: usize, discord_thread_prefix: &str) -> GitlabProject {
        GitlabProject {
            name: name.to_string(),
            id: id.to_string(),
            discord_thread_prefix: discord_thread_prefix.to_string(),
        }
    }
}

pub struct Handler {
    pub client: reqwest::Client,
    pub gitlab_token: String,
    pub gitlab_projects: HashMap<&'static str, GitlabProject>,
    pub periodic_task_context: Arc<RwLock<Option<Context>>>,
}

const REVIEW_STRING: &str = "review";
const APPROVE_STRING: &str = "approve";
const VERSION_STRING: &str = "labbot-version";

pub const VELOREN_SERVER_ID: u64 = 449602562165833758;

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum Changes {
    Code,
    Audio,
    Voxel,
}

impl Changes {
    fn role_name(&self) -> &'static str {
        match self {
            Changes::Code => "Code Reviewer",
            Changes::Audio => "Audio Reviewer",
            Changes::Voxel => "Asset Reviewer",
        }
    }

    fn from_file_ext(ext: &str) -> Self {
        match ext {
            "vox" => Changes::Voxel,
            "wav" => Changes::Audio,
            _ => Changes::Code,
        }
    }
}

pub struct GitlabMR {
    title: String,
    // TODO: @perf could use an enum set here.
    changes: HashSet<Changes>,
}

impl Handler {
    async fn interaction_create(
        &self,
        context: Context,
        interaction: Interaction,
    ) -> Result<(), Box<dyn std::error::Error>> {
        // Get the slash command, or return if it's not a slash command.
        let command = if let Interaction::Command(command) = interaction {
            command
        } else {
            return Ok(());
        };

        if let Err(e) = command.channel_id.to_channel(&context).await {
            warn!("Error getting channel: {:?}", e);
        };

        match &command.data.name[..] {
            REVIEW_STRING => {
                let merge_request_number = &command
                    .data
                    .options
                    .first()
                    .expect("Expected int option")
                    .value;

                let gitlab_project_name_option = command.data.options.get(1);

                let gitlab_project_name = get_gitlab_project_name(gitlab_project_name_option)
                    .ok_or("Gitlab project name isn't a string")?;

                let gitlab_project = self
                    .get_gitlab_project(context.clone(), command.clone(), gitlab_project_name)
                    .await?
                    .ok_or(format!(
                        "Unknown Gitlab project name: {}",
                        gitlab_project_name
                    ))?;

                if let CommandDataOptionValue::Integer(number) = merge_request_number {
                    self.open_discord_mr_thread(context, command.clone(), *number, gitlab_project)
                        .await?
                } else {
                    warn!("Merge request isn't a number");
                    return Ok(());
                }
            }
            APPROVE_STRING => {
                let merge_request_number = &command
                    .data
                    .options
                    .first()
                    .expect("Expected int option")
                    .value;

                let gitlab_project_name_option = command.data.options.get(1);

                let gitlab_project_name = get_gitlab_project_name(gitlab_project_name_option)
                    .ok_or("Gitlab project name isn't a string")?;

                let gitlab_project = self
                    .get_gitlab_project(context.clone(), command.clone(), gitlab_project_name)
                    .await?
                    .ok_or(format!(
                        "Unknown Gitlab project name: {}",
                        gitlab_project_name
                    ))?;

                if let CommandDataOptionValue::Integer(number) = merge_request_number {
                    self.approve_mr(context, command.clone(), *number, gitlab_project)
                        .await?
                } else {
                    warn!("Merge request isn't a number");
                    return Ok(());
                }
            }
            VERSION_STRING => {
                command
                    .create_response(
                        &context.http,
                        CreateInteractionResponse::Message(
                            CreateInteractionResponseMessage::new().content(
                                MessageBuilder::new()
                                    // This will show an error with
                                    // rust-analyzer, but it compiles just fine
                                    // https://github.com/rust-analyzer/rust-analyzer/issues/6835
                                    //
                                    // Example output: `git:efe04ac-modified`
                                    .push(git_version!(prefix = "git:", fallback = "unknown"))
                                    .build(),
                            ),
                        ),
                    )
                    .await?;
            }
            _ => {
                warn!("should not happen");
                return Ok(());
            }
        }

        Ok(())
    }

    async fn get_gitlab_project(
        &self,
        context: Context,
        application_command: CommandInteraction,
        gitlab_project_name: &str,
    ) -> Result<Option<&GitlabProject>, Box<dyn std::error::Error>> {
        match self.gitlab_projects.get(gitlab_project_name) {
            Some(project) => Ok(Some(project)),
            None => {
                application_command
                    .create_response(
                        &context.http,
                        CreateInteractionResponse::Message(
                            CreateInteractionResponseMessage::new().content(format!(
                                "!{} is not a valid project",
                                gitlab_project_name
                            )),
                        ),
                    )
                    .await?;
                Ok(None)
            }
        }
    }

    async fn verify_mr(
        &self,
        merge_request_number: i64,
        gitlab_project: &GitlabProject,
    ) -> Result<Result<GitlabMR, String>, Box<dyn std::error::Error>> {
        // Query the GitLab API with the Veloren repo to find the MR
        let request = self
            .client
            .get(format!(
                "https://gitlab.com/api/v4/projects/{}/merge_requests/{}",
                gitlab_project.id, merge_request_number
            ))
            .build()?;
        let gitlab_response = self.client.execute(request).await?;
        let gitlab_response_body = gitlab_response.text().await?;
        let body_json: Value = from_str(&gitlab_response_body)?;

        // If the API call didn't return a valid title, send a response
        // to the channel
        if body_json["title"] == Value::Null {
            return Ok(Err(format!(
                "Error: !{} is not a valid Merge Request in !{}",
                merge_request_number, gitlab_project.name
            )));
        }

        // If the MR isn't opened, respond with an error. This is to try
        // and prevent people from accidentally creating threads on dead
        // issues
        if body_json["state"] != "opened" {
            return Ok(Err(format!(
                "Error: !{} is not an opened Merge Request in !{}",
                merge_request_number, gitlab_project.name
            )));
        }
        tracing::info!("verified that MR {merge_request_number} is still open");

        let mut changes = HashSet::new();

        {
            // Query the GitLab API to get the MR diff.
            let request = self
                .client
                .get(format!(
                    "https://gitlab.com/api/v4/projects/{}/merge_requests/{}/diffs",
                    gitlab_project.id, merge_request_number
                ))
                .build()?;
            let gitlab_response = self.client.execute(request).await?;
            let gitlab_response_body = gitlab_response.text().await?;
            let body_json: Value = from_str(&gitlab_response_body)?;

            if let Some(diffs) = body_json.as_array() {
                for diff in diffs {
                    if let Some((_, old_ext)) =
                        diff["old_path"].as_str().and_then(|s| s.rsplit_once('.'))
                    {
                        changes.insert(Changes::from_file_ext(old_ext));
                    }
                    if let Some((_, new_ext)) =
                        diff["new_path"].as_str().and_then(|s| s.rsplit_once('.'))
                    {
                        changes.insert(Changes::from_file_ext(new_ext));
                    }
                }
            } else {
                error!("Expected an array to be returned for diffs");
            }
        }

        Ok(Ok(GitlabMR {
            title: body_json["title"].to_string(),
            changes,
        }))
    }

    async fn verify_role(
        &self,
        context: &Context,
        application_command: &CommandInteraction,
        role_name: &str,
    ) -> Result<(GuildId, Guild), Box<dyn std::error::Error>> {
        let guild_id = application_command.guild_id.ok_or("Cannot get guild_id")?;
        let guild = guild_id
            .to_guild_cached(context)
            .ok_or("Cannot get guild")?
            .to_owned();

        // Make sure the author is at least a contributor
        if let Some(role) = guild.role_by_name(role_name) {
            if application_command
                .user
                .has_role(&context.http, guild_id, role)
                .await?
            {
                Ok((guild_id, guild.to_owned()))
            } else {
                application_command
                    .create_response(
                        &context.http,
                        CreateInteractionResponse::Message(
                            CreateInteractionResponseMessage::new().content(format!(
                                "You need to be a {} to use this command",
                                role_name
                            )),
                        ),
                    )
                    .await?;
                None.ok_or("user doesnt has role")?
            }
        } else {
            warn!(?role_name, "role doesnt exist");
            None.ok_or("couldn't verify role")?
        }
    }

    async fn open_discord_mr_thread(
        &self,
        context: Context,
        application_command: CommandInteraction,
        merge_request_number: i64,
        gitlab_project: &GitlabProject,
    ) -> Result<(), Box<dyn std::error::Error>> {
        // Verify that the MR is open
        let mr = self.verify_mr(merge_request_number, gitlab_project).await?;
        let mr = match mr {
            Ok(mr) => mr,
            Err(content) => {
                application_command
                    .create_response(
                        &context.http,
                        CreateInteractionResponse::Message(
                            CreateInteractionResponseMessage::new().content(content),
                        ),
                    )
                    .await?;
                return Ok(());
            }
        };

        // Make sure that the user is a Contributor to create thread
        let (_guild_id, guild) = self
            .verify_role(&context, &application_command, "Contributor")
            .await?;

        // If there is already a thread with the MR number, return a message
        // that links to it
        let mr_threads = get_merge_request_threads(guild.clone(), gitlab_project)?;
        if let Some(thread) = mr_threads.get(&merge_request_number) {
            application_command
                .create_response(
                    &context.http,
                    CreateInteractionResponse::Message(
                        CreateInteractionResponseMessage::new().content(
                            MessageBuilder::new()
                                .push(format!(
                                    "A thread for MR {} of {} already exists here: ",
                                    merge_request_number, gitlab_project.name,
                                ))
                                .mention(thread)
                                .build(),
                        ),
                    ),
                )
                .await?;
            return Ok(());
        }

        let reviewer_roles = mr
            .changes
            .iter()
            .filter_map(|change| guild.role_by_name(change.role_name()));
        // Now that all errors are handled, we can create the thread
        let merge_request_response = MessageBuilder::new()
            .push("MR Thread for ")
            .push(mr.title.clone())
            .push(" created by ")
            .mention(&application_command.user.id)
            .build();

        application_command
            .create_response(
                &context.http,
                CreateInteractionResponse::Message(
                    CreateInteractionResponseMessage::new().content(merge_request_response),
                ),
            )
            .await?;
        let response_message = application_command.get_response(&context).await?;

        // Set the time for the thread to live before auto archive to the
        // max possible based on the tier of the server
        let archive_duration = match guild.premium_tier {
            PremiumTier::Tier0 => 1440,
            PremiumTier::Tier1 => 4320,
            PremiumTier::Tier2 => 10080,
            PremiumTier::Tier3 => 10080,
            _ => 1440,
        };

        let mut thread_title = format!(
            "{}{} {}",
            gitlab_project.discord_thread_prefix, merge_request_number, mr.title
        );

        const DISCORD_THREAD_TITLE_MAX_LENGTH: usize = 100;

        thread_title.truncate(DISCORD_THREAD_TITLE_MAX_LENGTH);

        let new_mr_thread = response_message
            .channel_id
            .create_thread_from_message(
                &context,
                response_message.id,
                CreateThread::new(thread_title)
                    .auto_archive_duration(AutoArchiveDuration::Unknown(archive_duration)),
            )
            .await?;

        // Send the message that pings the parties involved in the MR
        let thread_ping_message = new_mr_thread
            .send_message(
                &context,
                CreateMessage::new().content({
                    let mut msg = MessageBuilder::new();
                    msg.push("MR: ")
                        .push(mr.title.clone())
                        .push(" is ready for review! \n");

                    for role in reviewer_roles {
                        msg.mention(role);
                    }

                    msg.push("\n\n")
                        .push(format!(
                            "https://gitlab.com/veloren/{}/-/merge_requests/{} ",
                            gitlab_project.name, merge_request_number,
                        ))
                        .build()
                }),
            )
            .await?;

        // Post a link to the discord thread in the Gitlab MR
        let note_message = format!("MR discussion has been started by discord user {} [here](https://discord.com/channels/{}/{})",
                application_command.user.name,
                application_command.guild_id.ok_or("Cannot get guild")?,
                new_mr_thread.id.get());
        self.create_gitlab_note(&note_message, merge_request_number, gitlab_project)
            .await?;

        // Pin the message
        thread_ping_message.pin(&context).await?;
        Ok(())
    }

    async fn approve_mr(
        &self,
        context: Context,
        application_command: CommandInteraction,
        merge_request_number: i64,
        gitlab_project: &GitlabProject,
    ) -> Result<(), Box<dyn std::error::Error>> {
        // Verify that the MR is open
        let mr = self.verify_mr(merge_request_number, gitlab_project).await?;
        let _ = match mr {
            Ok(title) => title,
            Err(content) => {
                application_command
                    .create_response(
                        &context,
                        CreateInteractionResponse::Message(
                            CreateInteractionResponseMessage::new().content(content),
                        ),
                    )
                    .await?;
                return Ok(());
            }
        };

        // Make sure that the user is a Contributor to approve
        let (_guild_id, _guild) = self
            .verify_role(&context, &application_command, "Contributor")
            .await?;

        // Post a note on that MR
        let note = format!(
            "Approved by Discord user: {}",
            application_command.user.name
        );
        self.create_gitlab_note(&note, merge_request_number, gitlab_project)
            .await?;

        // Now that all errors are handled, we can approve a mr
        let request = self
            .client
            .post(format!(
                "https://gitlab.com/api/v4/projects/{}/merge_requests/{}/approve",
                gitlab_project.id, merge_request_number
            ))
            .header("PRIVATE-TOKEN", &self.gitlab_token)
            .build()
            .inspect_err(|error| tracing::error!(?error, "Failed to build approve request",))?;
        let gitlab_response = self.client.execute(request).await?;
        let message_content;
        if let Err(error) = gitlab_response.error_for_status_ref() {
            let gitlab_response_body = gitlab_response.text().await?;
            tracing::error!(
                ?error,
                ?merge_request_number,
                "Error while approving MR: {gitlab_response_body}"
            );
            message_content = MessageBuilder::new()
                .push("Could not approve MR, check the logs for more details")
                .build();
        } else {
            message_content = MessageBuilder::new()
                .push(format!("MR !{} approved", merge_request_number))
                .mention(&application_command.user.id)
                .build();
        }

        application_command
            .create_response(
                &context,
                CreateInteractionResponse::Message(
                    CreateInteractionResponseMessage::new().content(message_content),
                ),
            )
            .await?;

        Ok(())
    }

    async fn create_gitlab_note(
        &self,
        message: &str,
        merge_request_number: i64,
        gitlab_project: &GitlabProject,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let request = self
            .client
            .post(format!(
                "https://gitlab.com/api/v4/projects/{}/merge_requests/{}/notes",
                gitlab_project.id, merge_request_number
            ))
            .header(reqwest::header::CONTENT_TYPE, "application/json")
            .header("PRIVATE-TOKEN", &self.gitlab_token)
            .body(format!(r#"{{ "body": "{}" }}"#, message))
            .build()
            .inspect_err(|e| {
                tracing::info!(?e, "failed to generate request");
            })?;
        let gitlab_response = self.client.execute(request).await?;
        if let Err(error) = gitlab_response.error_for_status_ref() {
            let gitlab_response_body = gitlab_response.text().await?;
            tracing::error!(
                ?error,
                "Error while posting note to MR: {gitlab_response_body}"
            );
        }

        Ok(())
    }
}

fn get_gitlab_project_name(gitlab_project_name_option: Option<&CommandDataOption>) -> Option<&str> {
    if let Some(gitlab_project_name) = gitlab_project_name_option {
        if let CommandDataOptionValue::String(name) = &gitlab_project_name.value {
            Some(name)
        } else {
            warn!("Gitlab project name isn't a string");
            None
        }
    } else {
        Some("veloren")
    }
}

pub fn get_merge_request_threads(
    guild: Guild,
    gitlab_project: &GitlabProject,
) -> Result<HashMap<i64, GuildChannel>, Box<dyn std::error::Error>> {
    let re = Regex::new(&format!(
        r"^{}(\d{{1,5}}) .*$",
        gitlab_project.discord_thread_prefix
    ))
    .unwrap();
    let threads = guild
        .threads
        .iter()
        .filter_map(|thread| {
            let mut cap = re.captures_iter(&thread.name);
            if let Some(number) = cap.next() {
                return Some((number[1].parse::<i64>().unwrap(), thread.clone()));
            }
            None
        })
        .collect::<HashMap<i64, GuildChannel>>();

    Ok(threads)
}

#[async_trait]
impl EventHandler for Handler {
    async fn interaction_create(&self, context: Context, interaction: Interaction) {
        if let Err(e) = self.interaction_create(context, interaction).await {
            error!(?e, "Error while processing message");
        }
    }

    async fn ready(&self, context: Context, ready: Ready) {
        let name = &ready.user.name;
        info!(?name, "is connected!");

        if let Err(e) = GuildId::new(VELOREN_SERVER_ID)
            .set_commands(&context.http, vec![
                // Command to create a thread in a channel and ping @Code
                // Reviewers. It requires the @Contributor role, and will
                // get the name of the MR from the Gitlab API.
                CreateCommand::new("review")
                    .description("Create a thread in this channel and ping @Code Reviewers. Requires the @Contributor role.")
                    .add_option(
                        CreateCommandOption::new(CommandOptionType::Integer, "id", "The MR number to review")
                            .required(true)
                    )
                    .add_option({
                        let mut option = CreateCommandOption::new(CommandOptionType::Integer, "project", "The project the MR belongs to")
                            .required(true);
                        for project_name in self.gitlab_projects.keys() {
                            option = option.add_string_choice(*project_name, *project_name);
                        }
                        option
                    }),
                // Approve (or revoke) command to get the bot to comment on
                // an MR. Requires the @Contributor role.
                CreateCommand::new("Approve")
                    .description("Add an approval comment to an MR. Requires the @Contributor role.")
                    .add_option(
                        CreateCommandOption::new(CommandOptionType::Integer, "id", "The MR number to approvee")
                            .required(true)
                    )
                    .add_option({
                        let mut option = CreateCommandOption::new(CommandOptionType::Integer, "project", "The project the MR belongs to")
                            .required(true);
                        for project_name in self.gitlab_projects.keys() {
                            option = option.add_string_choice(*project_name, *project_name);
                        }
                        option
                    }),
                    // Get the current Git version that labbot is running on
                    CreateCommand::new("labbot-version")
                        .description("Check the Git commit that labbot is on")
            ])
            .await
        {
            error!(?e, "Error while creating the review command");
        }
    }

    // This mostly came from the Serenity docs
    // https://github.com/serenity-rs/serenity/blob/current/examples/e13_parallel_loops/src/main.rs
    async fn cache_ready(&self, context: Context, _guilds: Vec<GuildId>) {
        trace!("Cache built successfully!");
        *self.periodic_task_context.write().await = Some(context);
    }
}
