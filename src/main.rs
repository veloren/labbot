use reqwest::header::HeaderValue;
use serenity::{all::ApplicationId, prelude::*};
use std::collections::HashMap;
use std::env;
use std::sync::Arc;
use tokio::sync::RwLock;
use tracing::{debug, error, Level};
use tracing_subscriber::EnvFilter;

mod discord_handler;
mod periodic_tasks;
use discord_handler::{GitlabProject, Handler};
use periodic_tasks::PeriodicTasker;

const LABBOT_ID: u64 = 870294415141376030;

fn init_tracing() {
    let append_info = |mut f: EnvFilter, list: &[&str], level: &str| {
        for l in list {
            f = f.add_directive(format!("{}={}", l, level).parse().unwrap());
        }
        f
    };

    let list = &[
        "tokio_util",
        "h2",
        "rustls",
        "serenity",
        "tungstenite",
        "async_tungstenite",
        "hyper",
        "trust_dns_resolver",
        "trust_dns_proto",
        "reqwest",
        "mio",
        "want",
        "kube",
        "tower",
    ];

    let filter = EnvFilter::from_default_env();
    let filter = append_info(filter.add_directive(Level::TRACE.into()), list, "info");

    tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .with_env_filter(filter)
        .try_init()
        .unwrap();

    debug!("tracing initialized");
}

#[tokio::main]
async fn main() {
    init_tracing();

    // Configure the client with your Discord bot token in the environment.
    let token = env::var("DISCORD_TOKEN").expect("Expected DISCORD_TOKEN token in the environment");
    let gitlab_token =
        env::var("GITLAB_TOKEN").expect("Expected GITLAB_TOKEN token in the environment");

    if HeaderValue::from_str(&gitlab_token).is_err() {
        tracing::error!(
            "Gitlab token contains characters, the value contain only visible ASCII characters (32-127). Invalid characters: {}",
            gitlab_token
                .chars()
                .enumerate()
                .filter(|(_, c)| ((*c as u8) < 32 && *c != '\t') || *c as u8 == 127)
                .map(|(index, c)| format!("'{c:?}' at {index}"))
                .collect::<Vec<_>>()
                .join(", ")
        );
        return;
    }

    let shared_context = Arc::new(RwLock::new(None));

    let gitlab_projects = HashMap::from([
        ("veloren", GitlabProject::new("veloren", 10174980, "V")),
        ("rfcs", GitlabProject::new("rfcs", 7495284, "RF")),
        (
            "airshipper",
            GitlabProject::new("airshipper", 21423598, "AS"),
        ),
        ("book", GitlabProject::new("book", 11399557, "B")),
        ("site", GitlabProject::new("site", 7926160, "S")),
        ("auth", GitlabProject::new("auth", 13768223, "AU")),
        ("torvus", GitlabProject::new("torvus", 11913537, "T")),
        (
            "veloren-docker-ci",
            GitlabProject::new("veloren-docker-ci", 11997468, "D"),
        ),
        ("labbot", GitlabProject::new("labbot", 28474023, "L")),
        (
            "serverbrowser",
            GitlabProject::new("serverbrowser", 39112038, "SB"),
        ),
        (
            "moderation-tool",
            GitlabProject::new("moderation-tool", 51453328, "MT"),
        ),
    ]);

    let discord_handler = Handler {
        client: reqwest::Client::new(),
        gitlab_token,
        gitlab_projects: gitlab_projects.clone(),
        periodic_task_context: Arc::clone(&shared_context),
    };

    let intents = GatewayIntents::GUILDS
        | GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::GUILD_INTEGRATIONS
        | GatewayIntents::GUILD_SCHEDULED_EVENTS;

    let mut client = Client::builder(&token, intents)
        .application_id(ApplicationId::new(LABBOT_ID))
        .event_handler(discord_handler)
        .await
        .expect("Err creating client");
    let periodic_tasks = PeriodicTasker {
        client: reqwest::Client::new(),
        gitlab_projects,
        context: shared_context,
    };

    let (client_result, periodic_tasks_result) = tokio::select! {
        e = client.start() => (Some(e), None),
        e = periodic_tasks.run() => (None, Some(e)),
    };
    if let Some(Err(e)) = client_result {
        error!(?e, "client error");
    }
    if let Some(Err(e)) = periodic_tasks_result {
        error!(?e, "periodic_tasks error");
    }
}
